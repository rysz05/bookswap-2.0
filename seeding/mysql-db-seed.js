const faker = require("faker");
const Seeder = require("mysql-db-seed").Seeder;
require('dotenv').config();
const db = require("../models");
const User = db.user;
const bcrypt = require('bcrypt');
const mysql = require('mysql');
faker.locale = "en";

class mySeeder extends Seeder {
	constructor(limit, host, port, user, password, database, locale="en") {
		super(limit, host, user, password, database, locale= "en");
		this.port = port
		this.connection = mysql.createPool({
	      connectionLimit: limit, // default = 10
	      host: host,
	      port: port,
	      user: user,
	      password: password,
	      database: database
	    });
	}
}

const seed = new mySeeder(
  10,
  process.env.DB_HOST,
  process.env.DB_PORT,
  process.env.DB_USER,
  process.env.DB_PASS,
  process.env.DB_DATABASE,
);

	(async () => {
	  await seed.seed(
	    30,
	    "users",
	    {
		    username: faker.name.firstName,
		    email: faker.internet.email,
		    img_path: faker.image.people,
		    password: await bcrypt.hash("p", 10),
		    city: faker.address.city,
		    street: faker.address.streetName,
		    building_nr: faker.datatype.number,
		    lat: faker.address.latitude,
		    lng: faker.address.longitude,
		    phone_nr: faker.phone.phoneNumber,
/*		    created_at: seed.nativeTimestamp(),
		    updated_at: seed.nativeTimestamp(),*/
		    rating: 3
	    }
	  )
	  await seedBooks()
	  seed.exit();
	  process.exit();
	})();

	async function seedBooks() {
		try {
			let idArray = await User.findAll()
				console.log(idArray)
			for (let el of idArray) {
			  await seed.seed(
			    5,
			    "books",
			    {
				    author: faker.name.findName,
				    title: faker.lorem.words,
				    genre: "fantasy",
				    quote: faker.lorem.paragraph,
				    book_condition: "good",
				    status: "free",
				    userId: el.id,
/*				    created_at: seed.nativeTimestamp(),
				    updated_at: seed.nativeTimestamp()*/
			    }
			  )

			}

		  seed.exit();
		} catch (e) {console.log(e)}
		  process.exit();
		}


