'use strict';

var dbm;
/*var type;
var seed;*/

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
/*  type = dbm.dataType;
  seed = seedLink;*/
};

exports.up = function(db) {
 // if (db.wishlistbook){db.dropTable('wishlistbook')}
  return db.createTable('wishlistbook', {
  	id: {type:'int', primaryKey: true, autoIncrement: true, notNull: true},
  	user: {type: 'int', notNull: true, foreignKey: {
          name: 'users_id',
          table: 'users',
            rules: {
              onDelete: 'CASCADE',
              onUpdate: 'RESTRICT'
            },
          mapping: 'id'
      }},
    author: {type: 'string', notNull: true},
    title: {type: 'string', notNull: true},
  	status: {type:'string', notNull: true, defaultValue: 'toBeRead'},
    orderId: {type:'int', notNull: true}
  })
};

exports.down = function(db) {
  return db.dropTable('wishlistbook')
};

exports._meta = {
  "version": 1
};
