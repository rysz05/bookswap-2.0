'use strict';

var dbm;
/*var type;
var seed;*/

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
/*  type = dbm.dataType;
  seed = seedLink;*/
};

exports.up = function(db) {
  if (db.books){db.dropTable('books')}
  return db.createTable('books', {
  	id: {type:'int', autoIncrement: true, primaryKey: true},
  	author: {type: 'string', notNull: true},
  	title: {type: 'string', notNull: true},
  	genre: {type: 'string'},
  	state: {type: 'string'},
  	quote: {type: 'text'},
  	book_condition: {type: 'string', defaultValue: "good"},
  	status: {type: 'string', notNull: true, defaultValue: 'free'},
  	user: {type: 'int', notNull: true, foreignKey: {
          name: 'user_id',
          table: 'users',
            rules: {
              onDelete: 'CASCADE',
              onUpdate: 'RESTRICT'
            },
          mapping: 'id'
      }},
    created_at: {type : 'string'},
    updated_at: {type : 'string'}
  })
};

exports.down = function(db) {
  return db.dropTable('books');
};

exports._meta = {
  "version": 1
};
