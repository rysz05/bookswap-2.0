'use strict';

var dbm;
/*var type;
var seed;*/

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
/*  type = dbm.dataType;
  seed = seedLink;*/
};

exports.up = function(db) {
  if (db.messages) {db.dropTable('messages')}
  return db.createTable('messages', {
  	id: {type:'int', primaryKey: true, autoIncrement: true, notNull: true},
  	text: {type:'text', notNull: true},
  	created_at: {type:'string'},
  	user: {type:'string', notNull: true},
  	chat_id: {type:'int',
			  foreignKey:{
			  		name:'chat_id',
			  		table:'chats',
			  		rules: {
			            onDelete: 'CASCADE',
			            onUpdate: 'RESTRICT'
			          },
			        mapping: 'id'
				}
			}
  })
};

exports.down = function(db) {
  return db.dropTable('messages')
};

exports._meta = {
  "version": 1
};
