//const mysql = require('mysql');
//export {}; // https://medium.com/@muravitskiy.mail/cannot-redeclare-block-scoped-variable-varname-how-to-fix-b1c3d9cc8206
//const pool = require('../database.js')
const axios = require('axios').default;
require('dotenv').config()
/**
Gets coordinates from address
@param {string} query - an address from users input
@returns {Object|null} Object including addresses latitude and longitude
*/
async function forwardGeocoding(query) {
	if (query) {
		const APIKey = process.env.GEOCODING_API_CODE
		let place = encodeURI(query)

		let APIurl = `https://api.opencagedata.com/geocode/v1/json?q=${place}&key=${APIKey}&countrycode=pl`
		try {
			const {data} = await axios({
				method: 'get',
				url: APIurl,
			})
			//console.log(data.results)
			if (data.results.length) {
				let address = data.results[0];
				let coordinates = address.geometry
				return coordinates
			} else {
				console.log('bad query')
			}
		} catch(e) {console.log(e)}
	} else {
		console.log('bad query')
		return null
	}
}
/**
Counts the distance from coordiante sets
@param {Object} users_coord - logged users coordiantes
@param {Object} other_coords - other users coordinates
@returns {number} DIstance in km between both places
*/
function haversine(users_coord, others_coords) {

	function toRad(x) {
		return x * Math.PI / 180;
	}

	var lat1 = users_coord.lat
	var lng1 = users_coord.lng
	var lat2 = others_coords.lat
	var lng2 = others_coords.lng
	/**
	 * [R description]
	 * @type {Number}
	 */
	var R = 6371; // km
	//has a problem with the .toRad() method below.
	var x1 = lat2-lat1;
	var dLat = toRad(x1);
	var x2 = lng2-lng1;
	var dLng = toRad(x2);
	var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
	        Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) *
	        Math.sin(dLng/2) * Math.sin(dLng/2);
	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
	var distance = R * c;

	return distance
}
/**
Counts the distance from coordiante sets
@param {Object} array - elements to check
@param {number} radius - distance picked by the user
@param {number} user_id - the logged in user
@returns {Object} All the elements in the selected distance range
*/
/*async function booksToDisplay(array, radius, user_id) {
	try {
	let user_coord = await pool.query(`SELECT lat, lng FROM users WHERE id = ${user_id}`)

		let displayedResult = []

		for(let i=0; i<array.length; i++) {

			let other_coord	= {
			lat : +array[i].lat,
			lng : +array[i].lng
			}

			let distance = haversine(user_coord[0], other_coord)
			if (distance < radius) {
				displayedResult.push(array[i])
			}
		}
		return displayedResult
	} catch (e) {console.log(e)}
}*/


module.exports = {
	forwardGeocoding,
	haversine,
	/*booksToDisplay*/

}
