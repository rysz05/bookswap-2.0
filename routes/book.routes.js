const bookController = require('../controllers/bookController')

module.exports = function(app) {

  app.use(function(req, res, next) {
/*    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );*/
    next();
  });

  app.get("/books/:userId", bookController.getAllBooks);

  app.get('/loadSuggestions/:userId/:input', bookController.loadSuggestions)

  app.get("/usersBooks/:id", bookController.getUsersBooks)

  app.get("/getResults/:userId/:ask", bookController.getAskResult)

  app.post("/addBook", bookController.addBook)

  app.post("/updateBook", bookController.updateBook)

  app.post("/deleteBook/:bookId", bookController.deleteBook)

  app.post("/postWish", bookController.postWish)

  app.post("/updateWishStatus", bookController.updateWishStatus)

  app.post("/updateIndexes", bookController.updateIndexes)

  app.get("/wishList/:userId", bookController.getUsersWishList)
};
