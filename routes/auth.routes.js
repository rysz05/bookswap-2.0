const { verifySignUp } = require("../middleware");
const controller = require("../controllers/authController");
const multer = require('multer');
const path = require('path');

const storage = multer.diskStorage({
  destination: './uploads',
  filename: function(req, file, cb) {
    //console.log(files)
    cb(null, file.originalname)
  }
})

const fileFilter = (req, file, cb) => {
  //reject a file
  if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
    cb(null, true);
  } else {
    cb(null, false);
  }
}

const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024*1024
  },
  fileFilter: fileFilter
});


module.exports = (app) => {

  app.use(function(req, res, next) {
/*    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );*/
    next();
  });

  app.post("/api/auth/signup", upload.any(),
    [
      verifySignUp.checkDuplicateUsernameOrEmail
    ],
      controller.registerUser
  );

  app.post("/api/auth/signin", controller.loginUser);
};
