//const { authJwt } = require("../middleware");
const authController = require("../controllers/authController");
const userController = require('../controllers/user.controller')

const multer = require('multer')

// MULTER CONFIG

const storage = multer.diskStorage({
  destination: './uploads',
  filename: function(req, file, cb) {
    //console.log(files)
    cb(null, file.originalname)
  }
})

const fileFilter = (req, file, cb) => {
  //reject a file
  if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
    cb(null, true);
  } else {
    cb(null, false);
  }
}

const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024*1024
  },
  fileFilter: fileFilter
});

// MULTER CONFIG

module.exports = function(app) {

  app.use(function(req, res, next) {
/*    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );*/
    next();
  });

  app.get('/user', authController.checkUser)

  app.get('/user_data/:id', userController.findUsersData)

  app.post("/update", upload.any(), userController.update)
};
