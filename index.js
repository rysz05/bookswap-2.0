const express = require('express');
const path = require('path');
const morgan = require('morgan')
const bodyParser = require("body-parser")
const cors = require("cors")
const app = express();
const db = require("./models")
const passport = require('passport')
const jwtSecret = process.env.JWT_SECRET;
const errorController = require('./controllers/errorController');

const cookieParser = require('cookie-parser')
app.use(express.json())
app.use(cookieParser(jwtSecret))

let corsOptions = {
	origin: "http://localhost:3000"
}
app.use(cors(corsOptions))
app.use(morgan('tiny'))

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// Serve the static files from the React app
app.use('/uploads', express.static('uploads'));
app.use(express.static(path.join(__dirname, 'public/index.html')));
db.sequelize.sync();

app.use(passport.initialize())
app.use(errorController);

app.get("/", (req, res) => {
	res.json({message: "Welcome"})
})
// An api endpoint that returns a short list of items
app.get('/api/getList', (req,res) => {
    var list = ["item1", "item2", "item3"];
    res.json(list);
    console.log('Sent list of items');
});

require("./routes/auth.routes")(app);
require("./routes/user.routes")(app);
require("./routes/book.routes")(app);
require("./routes/chat.routes")(app);

// Handles any requests that don't match the ones above
app.get('*', (req,res) =>{
    res.sendFile(path.join(__dirname+'/client/build/index.html'));
});

const port = process.env.PORT || 5000;
app.listen(port);

console.log('App is listening on port ' + port);




function initial() {
  Role.create({
    id: 1,
    name: "user"
  });

  Role.create({
    id: 2,
    name: "admin"
  });
}
