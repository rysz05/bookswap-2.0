
module.exports = (sequelize, Sequelize) => {
	const Message = sequelize.define("message", {
		id: {
			type: Sequelize.INTEGER,
			primaryKey: true,
			autoIncrement: true,
			allowNull:false,
		},
		text: {type: Sequelize.STRING, allowNull: false},
		user: {type: Sequelize.INTEGER, allowNull: false},
	});



	return Message;
};
