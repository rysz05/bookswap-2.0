
require('dotenv').config()

const { Sequelize } = require("sequelize");
const sequelize = new Sequelize(
	process.env.DB_DATABASE,
	process.env.DB_USER,
	process.env.DB_PASS,
	{
	  host: process.env.DB_HOST,
	  dialect: "mysql",
	  define: {
	  	timestamps: false
	  },
	  port: process.env.DB_PORT,
	  //operatorsAliases: false,

	  pool: {
	    max: 5,
	    min: 1,
	    acquire: 30000,
	    idle: 100000
	  }
	}
);

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.user = require("./user.model.js")(sequelize, Sequelize);
db.book = require("./book.model.js")(sequelize, Sequelize);
db.chat = require("./chat.model.js")(sequelize, Sequelize);
db.message = require("./message.model.js")(sequelize, Sequelize);
db.wishListBook = require("./wishListBook.model.js")(sequelize, Sequelize);


db.book.belongsTo(db.user);
db.message.belongsTo(db.chat);
db.wishListBook.belongsTo(db.user);

db.chat.belongsTo(db.user, {as: 'firstUser', foreignKey: 'first_user'});
db.chat.belongsTo(db.user, {as: 'secondUser', foreignKey: 'second_user'});
db.chat.belongsTo(db.book)

module.exports = db;
