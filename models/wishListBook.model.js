
module.exports = (sequelize, Sequelize) => {
	const WishListBook = sequelize.define("wishListBook", {
		id: {
			type: Sequelize.INTEGER,
			primaryKey: true,
			autoIncrement: true
		},
/*		user: {type: Sequelize.INTEGER, allowNull: true},*/
		author: {type: Sequelize.STRING, allowNull: false},
		title: {type: Sequelize.STRING, allowNull: false},
		status: {type: Sequelize.STRING, allowNull: false, defaultValue: 'toBeRead'},
		orderId: {type: Sequelize.INTEGER, allowNull: false}

/*		created_at: {type: Sequelize.DATE},
		updated_at: {type: Sequelize.DATE}*/
	});



	return WishListBook;
};
