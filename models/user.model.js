module.exports = (sequelize, Sequelize) => {
	const User = sequelize.define("user", {
		id: {
			type: Sequelize.INTEGER,
			primaryKey: true,
			autoIncrement: true
		},
		username: {type: Sequelize.STRING, defaultValue: ""},
		email: {
			type: Sequelize.STRING,
			validate: {
				isEmail: true
			}
		},
		img_path: {type: Sequelize.STRING},
		password: {type: Sequelize.STRING},
		city: {type: Sequelize.STRING},
		street: {type: Sequelize.STRING},
		building_nr: {type: Sequelize.STRING},
		lat: {type: Sequelize.STRING},
		lng: {type: Sequelize.STRING},
		phone_nr: {type: Sequelize.STRING},
		accessTokenFB: {type: Sequelize.STRING},
		accessTokenGoogle: {type: Sequelize.STRING},
/*		createdAt: {
			allowNull: false,
			defaultValue: Sequelize.fn('now'),
			type: Sequelize.DATE
		},
		updatedAt:  {
			allowNull: false,
			defaultValue: Sequelize.fn('now'),
			type: Sequelize.DATE
		},*/
		rating: {type: Sequelize.INTEGER},
		authToken: {type: Sequelize.STRING},
		authStatus: {type: Sequelize.STRING},
		secureCode: {type: Sequelize.STRING},
		randNumb: {type: Sequelize.INTEGER}
	});

	return User;
};
