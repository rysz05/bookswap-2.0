
module.exports = (sequelize, Sequelize) => {
	const Chat = sequelize.define("chat", {
		id: {
			type: Sequelize.STRING,
			primaryKey: true,
			allowNull: false,
		},
		first_user: {type: Sequelize.INTEGER, allowNull: false},
		second_user: {type: Sequelize.INTEGER, allowNull: false},
		bookId: {type: Sequelize.INTEGER}
	});



	return Chat;
};
