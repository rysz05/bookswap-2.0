
module.exports = (sequelize, Sequelize) => {
	const Book = sequelize.define("book", {
		id: {
			type: Sequelize.INTEGER,
			primaryKey: true,
			autoIncrement: true
		},
		author: {type: Sequelize.STRING, allowNull: false},
		title: {type: Sequelize.STRING, allowNull: false},
		genre: {type: Sequelize.STRING},
		quote: {type: Sequelize.TEXT},
		book_condition: {type: Sequelize.STRING, defaultValue: 'good'},
		status: {type: Sequelize.STRING, allowNull: false, defaultValue: 'free'}
	});



	return Book;
};
