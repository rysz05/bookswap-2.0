// import axios from 'axios';
// import { createStore, applyMiddleware } from 'redux'
// import thunk from 'redux-thunk';
// import { dispatch } from 'rxjs/internal/observable/pairs';


// const rootReducer = (state = { dataLoaded: false }, action) => {
//     if (action.type === "toggle") {
//         return {
//             dataLoaded: !state.dataLoaded
//         }
//     }
//     if (action.type === "dataLoaded") {
//         return action.payload
//     }
//     return state;
// }
// // const store = createStore(rootReducer)
// const store = createStore(rootReducer, applyMiddleware(thunk));


// export async function fetchData(dispatch, getState) {
//     const response = await axios.get('https://jsonplaceholder.typicode.com/users')

//     const stateBefore = getState()
//     console.log('Data before dispatch: ', stateBefore)

//     dispatch({type: 'dataLoaded', payload: response})

//     const stateAfter = getState()
//     console.log('Data after dispatch: ', stateAfter.data)
// }

// export default store;
