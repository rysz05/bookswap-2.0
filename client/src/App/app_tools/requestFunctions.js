const axios = require('axios').default;

export default async  function menuGetData(id) {
    console.log('działa')
    try {
        const { data } = await axios.get(`http://localhost:5000/user_data/${id}`);
        return data
    } catch (error) {
        throw new Error(error.response.data || 'Could not get users data.');
    }
}

export async function shareGetBooks(userId) {
    try {
        const { data } = await axios.get(`http://localhost:5000/usersBooks/${userId}`);
        return data

    } catch (error) {
        throw new Error(error.response.data || 'Could not add book.');
    }
}

export async function wishListGetData(userId) {
    console.log('req functions', userId)
    try {
        const { data } = await axios.get(`http://localhost:5000/wishList/${userId}`);
        return data
    } catch (error) {
        throw new Error(error.response.data || 'Could not get Your wish list');
    }
}
