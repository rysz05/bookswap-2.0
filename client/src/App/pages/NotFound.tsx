import React from 'react';
import Header from '../components/header';

export default function NotFound() {
    return(
        <React.Fragment>
            <Header />
            <section className="container">
                You seem lost.
            </section>

        </React.Fragment>
    )
}
