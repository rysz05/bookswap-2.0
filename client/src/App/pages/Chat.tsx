import React, { ReactComponentElement } from 'react';
import Header from '../components/header';
import { RouteComponentProps } from 'react-router-dom';

type TParams =  { ChatId: string };

function Chat({ match }: RouteComponentProps<TParams>){

    const { params } = match
    console.log('chat id', params.ChatId)
    //bcrypt.compare


    return (
        <React.Fragment>
            <Header />
            <div className="container">
                <h1>Chat page</h1>
            </div>

        </React.Fragment>
    )
}

export default Chat
