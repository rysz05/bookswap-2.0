import React, { useState, useRef, useContext } from "react";
import { Link, Redirect } from 'react-router-dom';
import Form from "react-validation/build/form";
import CheckButton from "react-validation/build/button";
import Input from "react-validation/build/input";
import val from "../app_tools/validation.js"
import { UserContext } from '../hooks/UserContext';
import useAuth from './../hooks/useAuth';
import Error from './../components/Error';

const Login = () => {

  const form = useRef();
  const checkBtn = useRef();

  const [loading, setLoading] = useState(false);
  const [message, setMessage] = useState("");


  const initialUserState = {
    username: "",
    password: ""
  }
  const [user, setUser] = useState(initialUserState)

  const handleInputChange = event => {
    const { name, value } = event.target;
    setUser({ ...user, [name]: value });
  };

  const { loginUser, error } = useAuth();

  const handleLogin = async (e) => {

    e.preventDefault();
    setMessage('')
    setLoading(true)
    form.current.validateAll();

    if (checkBtn.current.context._errors.length === 0) {
      try {

         await loginUser(user);
        setLoading(false)
      } catch (error) {
        const resMessage =
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString();

          setLoading(false);
          setMessage(resMessage);
        }
      }
  }

  const { currentUser } = useContext(UserContext)
    if (currentUser) {
      <Redirect to='/home'/>
   }

  return (
    <div className="container mt-5">
      <div className="hello_header">
          <div className="row">
          <div className="col-8">
        <h1>Login</h1>
        <Form onSubmit={handleLogin} ref={form}>
          <div className="form-group">
            <label htmlFor="username">username</label>
          <Input
              type="text"
              className="form-control"
              id="inputUsername"
              name="username"
              value={user.username}
              onChange={handleInputChange}
              validations={[val.required, val.vusername]}
            />
          </div>
          <div className="form-group">
            <label htmlFor="password">password</label>
{            <Input
              type="password"
              className="form-control"
              id="password_input"
              name="password"
              value={user.password}
              onChange={handleInputChange}
              validations={[val.required, val.vpassword]}
            />}
            <div className="form-group">
{            <button className="btn btn-primary btn-block" disabled={loading}>
              {loading && (
                <span className="spinner-border spinner-border-sm mt-3"></span>
              )}
              <span>Login</span>
            </button>}
          </div>
            {message && (
            <div className="form-group">
              <div className="alert alert-danger" role="alert">
                {message}
              </div>
            </div>
          )}
          <CheckButton style={{ display: "none" }} ref={checkBtn} />
            <p><a href="/findYourAccount">Forgot Password?</a></p>
          </div>
                <div className="form-check">
        	  <input className="form-check-input" type="checkbox" value="lsRememberMe" id="rememberMe" />
        	  <label className="form-check-label" htmlFor="rememberMe">remember me</label>
        	</div>
{          <div className="form-group">
                 {error && <Error error={error.messages}/>}
          </div>}
    {/*      <button type="submit" href="" className="btn btn-primary mt-3" id="rememberMe_btn">Log in</button>*/}
        </Form>
        </div>
        <div className="col">
          <div className="mb-2">
            <button className="btn btn-outline-dark"><a href="/auth/facebook">Login with Facebook</a></button>
          </div>
          <div className="mb-2">
            <button className="btn btn-outline-dark"><a href="/auth/google">Login with Google</a></button>
          </div>
          <Link to="./Register">
            <button className="btn btn-lg btn-outline-dark">Register</button>
          </Link>
        </div>
          </div>
        </div>
    </div>
    )
}

export default Login
