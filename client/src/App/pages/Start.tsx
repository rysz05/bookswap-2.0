import { useContext } from 'react';
import { Link } from 'react-router-dom';
import useLogout from '../hooks/useLogout';
import { UserContext } from '../hooks/UserContext';

function Start() {

  const context = useContext(UserContext)
  const { logoutUser } = useLogout();

	return(

		<div className="container mt-5">
		  <nav className="navbar navbar-expand navbar-dark bg-dark">
        <div className="navbar-nav mr-auto">

          {context!.user && (
            <li className="nav-item">
              <Link to={"/Home"} className="nav-link">
                Main
              </Link>
            </li>
          )}
        </div>

        {context!.user ? (
          <div className="navbar-nav ml-auto">
            <li className="nav-item">
              <Link to={"/Menu"} className="nav-link">
                {context!.user.username}
              </Link>
            </li>
            <li className="nav-item">
              <a href="/login" className="nav-link" onClick={logoutUser}>
                LogOut
              </a>
            </li>
          </div>
        ) : (
          <div className="navbar-nav ml-auto">
            <li className="nav-item">
              <Link to={"/login"} className="nav-link">
                Login
              </Link>
            </li>
            <li className="nav-item">
              <Link to={"/register"} className="nav-link">
                Sign Up
              </Link>
            </li>
          </div>
        )}
      </nav>
		  <div className="hello_header">
			<div className=" row">
				<h1 className="h_logo mt-4 mb-5 col">Bookswap</h1>
			</div>
			<div className="t_box row">
				<div className="t_main col">
					<p>"Hello, this is an app for exchanging books. After registering you can search books by title or author, genre and within a selected distance. Selecting a book will send you to a chat window with the books owner where you can set the swap specifics.</p>
					<p>You add your own books by adding a pic of their barcode or manually writong all the info. You also have a place to write down all the books you're interested in reading in the future. Be nice and have fun!"</p>
				</div>
			</div>
	  </div>
		</div>
)}

export default Start
