import React, { useState, useEffect, useContext, useCallback } from 'react';
import { UserContext } from '../hooks/UserContext';
import Header from '../components/header';
import Autocomplete from '../components/Autocomplete';
import axios from 'axios';
import { useHistory } from 'react-router-dom'

let bcrypt = require("bcryptjs");

interface Book {
	id: number
	userId: number
	title: string
	author: string
	genre: string
	quote: string
	state: "good" | "bad" | "ugly"
	status: "free" | "reserved"
}

const Search:React.FC = () => {

	const history = useHistory();
	const currentUser= useContext(UserContext)
	const [books, setBooks] = useState<Book[]>([]);
	const [results, setResults] = useState<Book[]>([])
	const [userInput, setUserInput] = useState('')

	console.log('userInput search', userInput)
	console.log('books', books)

	const update = (input:string) => {
			console.log('update', input.length)
			setUserInput(input)
	}
	const loadSuggestions = useCallback(async (input) => {

		if (userInput.length > 2) {
			console.log('loadsuggestions', )
			try {
				let { data } = await axios.get(`http://localhost:5000/loadSuggestions/${currentUser!.user!.id}/${input}`);

				setBooks(data);
			} catch (err) {
				console.log(err)
			}
		}
	}, [userInput.length, currentUser!.user!.id])

	useEffect(() => {

		const stopClicking = setTimeout(() => {
			console.log('inside set timeout')
				loadSuggestions(userInput)

		}, 500);
		return () => {
			clearTimeout(stopClicking)
		}
	}, [userInput, loadSuggestions])

	const handleSubmit = async (autocompleteInput:string) => {
		try {
			let results = await axios.get(`http://localhost:5000/getResults/${currentUser!.user!.id}/${autocompleteInput}`);
									console.log('result from handleSubmit')
									console.log(results.data);
			setResults(results.data)
		} catch (err) {console.log(err)}
	}

	const ResultsList = () => {
		console.log(currentUser!.user!.id)
		console.log(results)

		const listItems = results.map((result) =>

				<button className="btn btn-link" onClick={() => setupChat(result.userId)} key={result.id.toString()}>

					<li className="list-group-item card" style={{ "width": "850px" }}>
						<div className="card-body border">
							<h4 className="card-subtitle">{result.author}</h4><h2 className="card-title">{result.title.charAt(0).toUpperCase() + result.title.slice(1)}</h2>
							<p>State: {result.state}</p>
							<p>Genre: {result.genre}</p>
							<p>status: {result.status}</p>
							<p className="card-text">{result.quote}</p>
						</div>
					</li>
				</button>

		)

		// console.log(listItems)
		return(
			<ul className="list-group list-group-flush">{listItems}</ul>
			)
	}

	const setupChat = (second_user:number) => {

		let chatId = bcrypt.hashSync(`${second_user} ${currentUser}`, 4)

		let chat = {
			first_user: currentUser!.user!.id,
			second_user: second_user,
			chatId: chatId
		}
		let res = axios.post(`http://localhost:5000/createChat`, chat)
		console.log(res)
		history.push(`/Chat/${chatId}`)
	}

return (
	<React.Fragment>
		<Header />
		<section className="container">
			<div className="row mt-5">
				<div className="col">
					<h2>Search for books by author or title</h2>
				</div>
			</div>
			<div className="row mt-3">
				<div className="col">
					<Autocomplete suggestions={books} onSubmit={handleSubmit} update={update}/>
				</div>
				</div>
			<div className="row mt-5">
				<div className="col">
					{results.length > 0 && <ResultsList />
					}
				</div>
			</div>
		</section>
	</React.Fragment>
	)
}
export default Search;
