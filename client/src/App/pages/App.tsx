import React, { Suspense } from 'react';
import { Route, Switch } from 'react-router-dom';
import { UserContext } from '../hooks/UserContext';
import useFindUser from '../hooks/useFindUser';
import PrivateRoute from '../components/PrivateRoute'
import './App.css';
import Home from './Home';
import Start from './Start';
import LoadingSpinner from '../components/UI/LoadingSpinner';

const Share = React.lazy(() => import('./Share'))
const Search = React.lazy(() => import('./Search'))
const Menu = React.lazy(() => import('./Menu'))
const Wish_list = React.lazy(() => import('./Wish_list'))
const Register = React.lazy(() => import('./Register'))
const Login = React.lazy(() => import('./Login'))
const AddABook = React.lazy(() => import('./AddABook'))
const Chat = React.lazy(() => import('./Chat'))
const NotFound = React.lazy(() => import('./NotFound'))


const App = () => {
  const { user, setUser, isLoading } = useFindUser();

  return (
    <div>
      <UserContext.Provider value={{ user, setUser, isLoading}}>
        <Suspense fallback={<LoadingSpinner />}>
        <Switch>
          <Route exact path='/' component={Start}/>
          <Route path='/Register' component={Register}/>
          <Route path='/Login' component={Login} />
          <PrivateRoute path='/Home' component={Home}/>
          <PrivateRoute path='/AddABook' component={AddABook}/>
          <PrivateRoute path='/Share' component={Share}/>
          <PrivateRoute path='/Search' component={Search}/>
          <PrivateRoute path='/Menu' component={Menu}/>
          <PrivateRoute path='/Wish_list' component={Wish_list}/>
          <PrivateRoute path='/Chat/:ChatId' component={Chat}/>
          <PrivateRoute path='*' component={NotFound} />
        </Switch>
        </Suspense>
       </UserContext.Provider>
    </div>
    )
}


export default App;
