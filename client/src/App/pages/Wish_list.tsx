import { Fragment, useContext, useEffect, useState } from 'react';
import Header from '../components/header';
import WishListAddForm from '../components/wishListAddForm';
import { UserContext } from '../hooks/UserContext';
import WishListBody from '../components/WishListBody';
import axios from 'axios';

interface Book {
    author: string,
    id: number,
    orderId: number
    status: "done" | "toBeRead",
    title: string,
    userId: number
}

function Wish_list() {

	const user = useContext(UserContext)
	// const { sendRequest, status, data: books= [] } = useHttp(wishListGetData, true);

	const [books, setBooks] = useState<Book[]>([])

	console.log('books', books)

	useEffect(() => {
		//sendRequest(user.id)
		(async function(){
			try {
				const { data } = await axios.get(`http://localhost:5000/wishList/${user!.user!.id}`);
				setBooks(data)

			} catch (error) {
				throw new Error(error || 'Could not get Your wish list');
			}
		})()

	},[user!.user!.id])

	const handleSubmit:(newWish: { data:Book }) => void = (newWish) => {

		const newBook = newWish.data
		setBooks([...books, newBook])
	}

	return (
		<Fragment>
			<Header />
			{/* {status === 'pending' && <LoadingSpinner />} */}
			<section className="container">
			<div className="row">
				<div className="col">
					<h2>My wishlist:</h2>
				</div>
			</div>
			<div className="row">
				<div className="col">
					<WishListAddForm handleSubmit={handleSubmit} listLength={books.length} />
				</div>
			</div>
			<div className="row">
				<WishListBody books={books}/>
			</div>

		  </section>
		</Fragment>
		)
}

export default Wish_list
