import React, { Fragment, useContext, useEffect } from 'react';
import { UserContext } from '../hooks/UserContext';
import useHttp from '../hooks/useHttp';
import Header from '../components/header';
import BookCard from '../components/BookCard';
import { Link } from 'react-router-dom';
import LoadingSpinner from '../components/UI/LoadingSpinner';
import { shareGetBooks } from '../app_tools/requestFunctions';

interface Book {
	id: string
	title: string
	author: string
	genre: string
	status: "free" | "reserved"
}

const Share: React.FC = () => {

	const user = useContext(UserContext)
	const { sendRequest, status, data: books } = useHttp(shareGetBooks, true);

	console.log('status', status)
	console.log('books', books)

	useEffect(() => {
		sendRequest(user!.user!.id)
	}, [sendRequest, user!.user!.id])

	const updatePage = () => {
		sendRequest(user!.user!.id)
	}


	const BookList: React.FC<{bookStatus:"free" | "reserved"}> = ({ bookStatus }) => {
		// console.log('book status', bookStatus.bookStatus)
		const filteredBooks = books.filter((el:Book) => el.status === bookStatus)
		// console.log('filtered books', filteredBooks)
		const listItems = filteredBooks.map((item:Book) =>
			<div key={item.id} className="col-sm-3">
					<BookCard
						id={item.id}
						title={item.title}
						author={item.author}
						genre={item.genre}
						status={item.status}
						// bookChange={sendRequest}
						update={updatePage}
					/>
			</div>
		)
		// console.log("list items", listItems)
		return (
			<Fragment>
				{listItems}
			</Fragment>
		)
	}

	return(
		<Fragment>
			<Header />
			{status === 'pending' && <LoadingSpinner />}
			<section className="container mb-5">
				<div className="row mt-5 mb-5">
				<div className="col-8 ">
					<h1 className="second-header">Your books</h1>
				</div>
				<div className="col-4">
					<div className="radio-container">
						<button className="btn btn-link radio-cont-label" disabled>Bookcase</button>
						{/* <Link to="./edit-view">
						    <button className=" btn btn-link radio-cont-label">Edit</button>
						</Link> */}
						</div>
					</div>
				</div>
				<div className="row">
					<div className="col-sm-3">
						<div className="card add-card ">
						    <Link to="./AddABook">
						        <button className="btn btn-lg btn-outline-dark"><i className="fas fa-plus fa-plus_box" /></button>
						    </Link>
						</div>
					</div>
					{books &&
					<BookList bookStatus="free"/>}
			    </div>
				<div className="row">
		      		<div className="mt-5 mb-5 ml-3">
						<h3 className="second-header">Books already reserved</h3>
					</div>
				</div>
				<div className="row">
					{books &&
					<BookList bookStatus="reserved" />}
				</div>
			</section>
		</Fragment>
	)
}

export default Share;
