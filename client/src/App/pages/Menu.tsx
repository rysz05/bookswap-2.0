import { useContext, useEffect, useCallback } from 'react';
import { UserContext } from '../hooks/UserContext';
import Header from '../components/header';
import LogoutBtn from '../components/LogoutBtn';
import ModalTemplate from '../components/ModalTemplate';
import AddressChangeModalBody from '../components/AddressChangeModalBody';
import PersoDataChangeModalBody from '../components/PersoDataChangeModalBody';
import { Image } from 'react-bootstrap';
import LoadingSpinner from '../components/UI/LoadingSpinner';
import useHttp from '../hooks/useHttp';
import menuGetData from '../app_tools/requestFunctions'


function Menu() {

	const context = useContext(UserContext)

	// function to put inside useHttp has to outside of the component
	// otherwise causes rerenders and infinite loop

	const { sendRequest, status, data: userData, error } = useHttp(menuGetData, true);

	console.log('status', status)
	console.log('error', error)
	console.log('userData', userData)

	useEffect(() => {
		sendRequest(context!.user!.id)
	}, [sendRequest, context!.user!.id])

	const updateHandler = useCallback(() => {
		sendRequest(context!.user!.id);
	}, [sendRequest, context!.user!.id]);


	return (
		<div>
			<Header />
			{status === 'pending' ? <LoadingSpinner />
			:
		  <section className="container bookcase-section menu-box">
				<div className="row menu-box-row user-info">
					<div className="col-12 mt-5 mb-4 text-center" id="persoModalBtn">
							<ModalTemplate
								btnName={
									<div>
										<div>
							            {userData.img_path ?
							            <Image src={userData.img_path} rounded className="img-thumbnail float-right" alt="profile" style={{"height":"80px"}} />
							            : <i className="far fa-user right user-icon"></i>
							            }
						            	</div>
										<div className="ml-3">
											<p>{userData.username}</p>
											<p>{userData.phone_nr}</p>
											<p>{userData.email}</p>
										</div>
									</div>
								}
								header="Change your personal info"
								ModalBody={<PersoDataChangeModalBody
									id={context!.user!.id.toString()}
											img_path={userData.img_path}
											phone_nr={userData.phone_nr}
											email={userData.email}
									update={updateHandler}
											/>}
								/>
					</div>
				</div>
				<div className="row center menu-box-row mt-3 mb-4">
					<div className="col text-center">
						<p className="num">{userData.booksNr}</p>
						<p>my books</p>
					</div>
		         		<div className="col text-center">
		        			<p className="num">to do</p>
		        			<p>books borrowed</p>
		        		</div>
					<div className="col text-center">
						<p className="num">{userData.chatsNr}</p>
						<p>contacts</p>
					</div>
				</div>
				<div className="row menu-box-row mt-3 mb-3">
					<div className="col menu_list_col text-center">
						<div className="mb-2">
							<ModalTemplate
								btnName="Change address"
								header="change the place you want to exchange books at"
								ModalBody={<AddressChangeModalBody
									id={context!.user!.id}
											city={userData.city}
											street={userData.street}
											building_nr={userData.building_nr}
									update={updateHandler}
											 />}
								/>
						</div>
						<div className="mb-2">
							<ModalTemplate btnName="Something" header="Something, something" ModalBody={<p></p>}/>
						</div>
						<div className="mb-2">
							<ModalTemplate btnName="Stats" header="Your rating" ModalBody={<p></p>}/>
						</div>
						<div className="mb-2">
							<ModalTemplate btnName="Friends" header="List of friends" ModalBody={<p></p>}/>
						</div>
						<div>
						</div>
					</div>
				</div>
				<div className="row logout-box">
        		<LogoutBtn />
				</div>
		  </section>
}
		</div>
		)
}

export default Menu
