import React, { useState, useContext, useRef } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { UserContext } from '../hooks/UserContext';
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import val from "../app_tools/validation.js"
import useAuth from '../hooks/useAuth';
import Error from '../components/Error';
import FileUploader from "../components/FileUploader";
import { Alert } from '../components/Alert';
import { alertService } from '../services/alert.service'


export default function Register() {

	const form = useRef();
	const checkBtn = useRef();

	const initialUserState = {
		id: null,
		username: '',
		email: '',
		password: '',
		r_password: '',
		phone_nr: '',
		img_path: null,
		city: '',
		street: '',
		building_nr: ''
	};

	const [user, setUser] = useState(initialUserState)
	const [selectedFile, setSelectedFile] = useState(null)
	const [identicalPasswords, setIdenticalPasswords] = useState(undefined)

	const handleInputChange = event => {

		const { name, value } = event.target;
		setUser({ ...user, [name]: value });

		if (name === "r_password") {
			if (user.password === value ) {setIdenticalPasswords(true)}
			else {setIdenticalPasswords(false)}
		}
	};

	const { registerUser, error } = useAuth();

	const handleRegister = async (e) => {
	   e.preventDefault();
	   console.log(selectedFile)

	   //setUser({...user, img_path: selectedFile})
	   form.current.validateAll();
	   console.log(user)

	   let formData = new FormData();

	   formData.append("username", user.username)
	   formData.append("email", user.email)
	   formData.append("password", user.password)
	   formData.append("phone_nr", user.phone_nr)
	   formData.append("city", user.city)
	   formData.append("street", user.street)
	   formData.append("building_nr", user.building_nr)
	   formData.append("img_path", selectedFile)
	   console.log(formData)
		 for (var key of formData.entries()) {
		        console.log(key[0] + ', ' + key[1]);
		    }
	   if (checkBtn.current.context._errors.length === 0) {
	   			   console.log(formData)
	   	try {
		   let res = await registerUser(formData);
		   if (res.status === 201) {
			   alertService.success('Successfull registration!', {
			   		autoClose: true,
			   		keepAfterRouteChange: true
			   	})
		   }
	   	} catch(error) {
	   		console.log(error)
	   		//alertService.error(error.response.data, {autoClose: true})}
	   }
	}
	}

  const { currentUser } = useContext(UserContext)
    if (currentUser) {
      <Redirect to='/home'/>
   }

	return (
		<div className="container mt-5">
		<Alert />
			<div  className="row">
				<Form onSubmit={handleRegister} ref={form} className="col" encType="multipart/form-data">
					<div className="col">
						<h3>Register</h3>
					    <div className="form-group">
					        <label htmlFor="username">username</label>
					        <Input
						        type="text"
						        className="form-control"
						        name="username"
						        onChange={handleInputChange}
						        value={user.username}
						        validations={[val.required, val.vusername]}
						    />
					    </div>
					    <div className="form-group">
					        <label htmlFor="email">email</label>
					        <Input
						        type="email"
						        className="form-control"
						        id="inputEmail"
						        name="email"
						        onChange={handleInputChange}
						        value={user.email}
						        validations={[val.required, val.validEmail]}
					        />
					    </div>
					    <div className="form-group">
					        <label htmlFor="password">password</label>
					        <Input
						        type="password"
						        className="form-control password_input"
						        name="password"
						        onChange={handleInputChange}
						        value={user.password}
						        validations={[val.required, val.vpassword]}
					        />
					    </div>
					    <div className="form-group">
					        <label htmlFor="r_password">repeat password</label>
					        <Input
					        type="password"
					        className="form-control r_password_input"
					        name="r_password"
					        onChange={handleInputChange}
					        value={user.r_password}
					        validations={[val.required]}
					        />
					    </div>
					    {identicalPasswords ? (
					    	<div className="alert alert-success" role="alert">
						       Good job.
						    </div>
					    	) : (
					    	<div className="alert alert-danger" role="alert">
						       Not identical yet.
						    </div>
					    	)}
					    <div className="form-group">
					        <label htmlFor="phone_nr">phone number</label>
					        <Input
						        type="text"
						        className="form-control"
						        id="phone_nr_input"
						        name="phone_nr"
						        onChange={handleInputChange}
						        value={user.phone_nr}
					        />
					    </div>
					    <div className="form-group">
					        <FileUploader
					        	onFileSelectSuccess={(file) => setSelectedFile(file)}
					        	onFileSelectError={({error}) => alert(error)}
					        />
					      </div>
					</div>
					<div className="col">
					      <h3>Address</h3>
					      <p>Add to search books by distance</p>
					      <div className="form-group">
					        <label htmlFor="city">city</label>
					        <Input
						        type="text"
						        className="form-control inputAddress"
						        name="city"
						        onChange={handleInputChange}
						        required value={user.city}
					        />
					      </div>
					      <div className="form-group">
					        <label htmlFor="street">street</label>
					        <Input
						        type="text"
						        className="form-control inputAddress"
						        name="street"
						        onChange={handleInputChange}
						        required value={user.street}
					        />
					      </div>
					      <div className="form-group">
					        <label htmlFor="building_nr">building number</label>
					        <Input
						        type="text"
						        className="form-control inputAddress"
						        name="building_nr"
						        onChange={handleInputChange}
						        required value={user.building_nr}
					        />
					      </div>
					       <div className="form-check">
					    	  <Input
					    	  className="form-check-input"
					    	  type="checkbox"
					    	  id="rulesAgreement"
					    	  required
					    	  />
					    	  <label className="form-check-label" htmlFor="defaultCheck1">
					    	  Agree to
					    	  <Link to=""> rules</Link>
					    	  </label>
					    	</div>
					    	<div className="form-group mb-5 mt-3">
				               <button className="btn btn-primary btn-block" type="submit">Sign Up</button>
				            </div>
				            <div className="form-group">
			                    {error && <Error error={error.messages}/>}
			                </div>
			      <CheckButton style={{display: "none"}} ref={checkBtn}  className="btn btn-primary mt-3" />
					</div>
				</Form>
					<div className="col mt-5">
					      <div className="mb-2">
					        <button className="btn btn-outline-dark" >
					        	Login with Facebook
					        </button>
					      </div>
					      <div className="mb-2">
					        <button className="btn btn-outline-dark">
							Login with Google
					        </button>
					      </div>
					      <div className="mb-2">
					      	<Link to="./Login">
					        	<button className="btn btn-lg btn-outline-dark">Login</button>
					        </Link>
					      </div>
					</div>

			</div>
		</div>
		)
}
