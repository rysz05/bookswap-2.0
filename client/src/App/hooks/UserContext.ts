import { createContext } from 'react'

interface User {
    id: number,
    username: string
}

export const UserContext = createContext<{
    user: User | null,
    setUser: (user:User) => void,

    isLoading: boolean
    } | null >(null)
