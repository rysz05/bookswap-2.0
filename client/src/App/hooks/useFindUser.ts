import { useState, useEffect } from 'react';
import axios from 'axios';

interface User {
   username: string
   id: number
}

export default function useFindUser() {
   const [user, setUser] = useState<User | null>(null);
   const [isLoading, setLoading] = useState(true);


useEffect(() => {
   async function findUser() {
     await axios.get('/user')
        .then(res => {
        setUser(res.data.currentUser);
        setLoading(false);
     }).catch(err => {
        setLoading(false);
        console.log(err)
    });
  }

   findUser();
}, []);


return {
   user,
   setUser,
   isLoading
   }
}
