import { useContext } from 'react';
import { UserContext } from './UserContext';
import { useHistory } from 'react-router-dom';
import { useCookies } from 'react-cookie';

export default function useLogout() {

    let history = useHistory();
    const {/* user,*/ setUser } = useContext(UserContext);
    const [/*cookies, setCookie, */removeCookie] = useCookies(['jwt']);

    const logoutUser = async () => {

        try {
            removeCookie('jwt')
            setUser(undefined)
            history.push('/');
        } catch (err) {console.log(err)}
    }
    return {
        logoutUser
    }
}
