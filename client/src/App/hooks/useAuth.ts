import { useContext } from 'react';
import { useHistory } from 'react-router-dom';
import axios from 'axios';
import { UserContext } from './UserContext';
import { useCookies } from 'react-cookie';
import { alertService } from '../services/alert.service'

interface userToRegister {
  username: string
  email: string
  password: string
  img_path: string
  city: string
  street: string
  building_nr: string
  phone_nr?: string
  accesTokenFB?: string
  accessTokenGmail?: string
  rating?: string
  authToken?: string
}

interface dataToLogin {
  username: string
  password: string
}

export default function useAuth() {

  let history = useHistory();
  const context = useContext(UserContext);

  const [cookies, setCookie, removeCookie] = useCookies(['jwt']);

  const API_URL = "http://localhost:5000/api/auth/";

//set user in context and push them home
  const setUserContext = async () => {

    try {
         const res = await axios.get('/user')
         context!.setUser(res.data.currentUser);
         history.push('/home');
    } catch (error) {
         alertService.error(error.response.data, {autoClose: true})
     }
  }

//register user
  const registerUser = async (data:userToRegister) => {
     //const { username, email, password, passwordConfirm } = data;
     console.log(data)
     try {
       let registered = await axios.post(API_URL + 'signup', data,
        {
          headers: {"Content-Type": "multipart/form-data"}
        }
      )

       setCookie('jwt', registered.data.token, { path: '/' });
        try {
          await setUserContext()
        } catch(error) {
          console.log(error)
          alertService.error(error.response.data, {autoClose: true})
        }
     } catch(error) {alertService.error(error.response.data, {autoClose: true})}
  };

//login user
  const loginUser = async (data:dataToLogin) => {

   const { username, password } = data;

    try {
       let posted = await axios.post(API_URL + "signin", {
          username, password
       })
       setCookie('jwt', posted.data.token, { path: '/' });
        try {
           await setUserContext();
         } catch(error) {
           alertService.error(error.response.data, {autoClose: true})
           }
    } catch(error) {alertService.error(error.response.data, {autoClose: true})}
  }

return {
   registerUser,
   loginUser
  }
}
