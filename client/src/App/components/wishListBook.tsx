import { Draggable } from 'react-beautiful-dnd';

interface Book {
    author: string,
    id: number,
    orderId: number
    status: "done" | "toBeRead",
    title: string,
    userId: number
}

const WishListBook:React.FC<{ book?: Book | undefined, index:number }> = (props) => {

    let id:string
    props.book? id = props.book?.id.toString() : id = ''
   // const id:string = props.book?.id.toString()


    // console.log('book props', props)
    // console.log('draggableId',id)
    return (

        <Draggable draggableId={id} index={props.index}>
            {(provided) => (
                <div className="border p-2 mb-1 bg-light text-dark"
                {...provided.draggableProps}
                {...provided.dragHandleProps}
                ref={provided.innerRef}
                >
                    <div className="list-inline">
                        <p className="list-inline-item">{props.book?.author}</p>
                        <p className="list-inline-item">{props.book?.title}</p>
                        <div className="btn btn-link">X</div>
                    </div>
                </div>
            )}
        </Draggable>
    )
}

export default WishListBook
