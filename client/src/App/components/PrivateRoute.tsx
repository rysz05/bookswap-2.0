import React, { useContext } from 'react';
import { Route, Redirect } from 'react-router-dom';
import { UserContext } from '../hooks/UserContext';
import Loading from './Loading';


export default function PrivateRoute(props:any) {

   const context = useContext(UserContext);
   const { component: Component, ...rest } = props;

   if(context!.isLoading) {
      return <Loading/>
   }
   if(context!.user){
      return ( <Route {...rest} render={(props) =>
           (<Component {...props}/>)
            }
         />
       )}
   //redirect if there is no user
   return <Redirect to='/login' />
}
