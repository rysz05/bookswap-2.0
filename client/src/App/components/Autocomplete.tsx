import React, { useState, Fragment, useEffect, useCallback, EventHandler } from 'react';
import './App.css';

interface Book {
	author: string
	title:string
}

const Autocomplete:React.FC<{ suggestions:Book[], update:(input:string)=>void, onSubmit:(input:string)=> void }> = (props) => {

	console.log('autocomplete props', props)
	let { suggestions } = props;

	// Make strings out of objects from props
	let books:string[] = []

	for (let i = 0; i < suggestions.length; i++) {
			let author = suggestions[i].author
			if (!books.includes(author)) books.push(author);
			let title = suggestions[i].title
			if (!books.includes(title)) books.push(title);
	}

	const [activeSuggestion, setActiveSuggestion] = useState(0);
	const [filteredSuggestions, setFilteredSuggestions] = useState<string[]>([]);
	const [showSuggestions, setShowsuggestions] = useState(false);
	const [userInput, setUserInput] = useState("");

	const updateSuggestions = useCallback(() => {
		const filteredSuggestions = books.filter(
			suggestions =>
				suggestions.toLowerCase().indexOf(userInput.toLowerCase()) > -1
		);
		setActiveSuggestion(0);
		setShowsuggestions(true);
		setFilteredSuggestions(filteredSuggestions)
	}, [userInput, suggestions])

	useEffect(() => {
		updateSuggestions()
	}, [updateSuggestions])


	const onChange = (e:React.FormEvent<HTMLInputElement>) => {

		setUserInput(e.currentTarget.value);

		if (e.currentTarget.value.length > 2) {
			console.log('fire loadSuggestions')
			props.update(e.currentTarget.value)
		}
		updateSuggestions()
	}

	const onClick = (e:React.MouseEvent<HTMLElement>) => {
		setActiveSuggestion(0);
		setFilteredSuggestions([]);
		setShowsuggestions(false);
		setUserInput(e.currentTarget.innerText)
	}

	const onKeyDown = (e:React.KeyboardEvent) => {
		if (e.keyCode === 13) {
			setActiveSuggestion(0);
			setShowsuggestions(false);
			setUserInput(filteredSuggestions[activeSuggestion])
		}
		else if (e.keyCode === 38) {
			if (activeSuggestion === 0) {
				return
			}
			setActiveSuggestion(activeSuggestion - 1)
		}
		else if (e.keyCode === 40) { // down arrow
			if (activeSuggestion - 1 === filteredSuggestions.length) {
				return
			}
			setActiveSuggestion(activeSuggestion + 1)
		}
	}
	let suggestionsListComponent

	if (showSuggestions && userInput.length) {
		if (suggestions.length) {
			suggestionsListComponent = (
				<ul className="suggestions">
					{filteredSuggestions.map((suggestion, index) => {
						// console.log(suggestion)
						let className;

						if(index === activeSuggestion) {
							className = "suggestion-active";
						}
						return (
							<li className={className} key={index} onClick={onClick}>
							{suggestion}
							</li>
						)
					})}
				</ul>
				)
		}  else if (userInput.length > 3) {
			suggestionsListComponent = (
				<div className="no-suggestions">
				<em>No suggestions available. </em>
				</div>
				);
		}
	else {
		<p>t</p>
	}
	}

	const handleSubmit = () => {
		props.onSubmit(userInput)
	}

	return(
		<Fragment>
			<div className="row">
				<div className="col-8">
			        <input
			          type="text"
			          onChange={onChange}
			          onKeyDown={onKeyDown}
			          value={userInput}
			          className="form-control"
			        />
			        {suggestionsListComponent}
					</div>
				<div className="col-3">
			        <button type="submit" className="btn btn-primary" onClick={handleSubmit}>Search</button>
				</div>
			</div>
	      </Fragment>
		)
}

export default Autocomplete
