import React, { useRef, useContext } from 'react';
import { UserContext } from '../hooks/UserContext';
import axios from 'axios';

interface Book {
    author: string,
    id: number,
    orderId: number
    status: "done" | "toBeRead",
    title: string,
    userId: number
}

const WishListAddForm: React.FC<{ handleSubmit:(result:any) => void, listLength:number }> = ({ handleSubmit, listLength }) => {

    const user = useContext(UserContext)
    const authorRef = useRef<HTMLInputElement>(null)
    const titleRef = useRef<HTMLInputElement>(null)

    const submitNewWish = async (e: React.FormEvent) => {
        e.preventDefault()
        console.log('submit new wish')
        if (authorRef.current?.value !== '' && titleRef.current?.value !== '') {
            const request = {
                userId: user!.user!.id,
                author: authorRef.current!.value,
                title: titleRef.current!.value,
                orderId: listLength + 1
            }
            console.log('request from add Form', request)
            try {
                let result:Book = await axios.post(`http://localhost:5000/postWish`, request);
                console.log('result', result)
                handleSubmit(result)

                authorRef.current!.value = '';
                titleRef.current!.value = ''
            } catch (error) {
                throw new Error(error.response.data || 'Could not add book to wish list.');
            }
        }

    }
    return (
        <form
            className="form-inline"
            onSubmit={submitNewWish}
        >
            <button className="btn btn-primary mb-2">Add to the wishlist</button>
            <div className="form-group mx-sm-3 mb-2">
                <label className="sr-only">Author</label>
                <input
                    ref={authorRef}
                    className="form-control"
                    type="text"
                    name="author"
                    required={true}
                    placeholder="Author" />
            </div>
            <div className="form-group mx-sm-3 mb-2">
                <label className="sr-only">Title</label>
                <input
                    ref={titleRef}
                    className="form-control"
                    type="text"
                    name="title"
                    required={true}
                    placeholder="Title" />
            </div>
        </form>
    )
}

export default WishListAddForm

