import { Link } from 'react-router-dom';

const Header = () => {

	return (
	<header className="container">
	    <div className="row mt-4 mb-3">
		    <div className="col-9">
			    <Link to="./Home">
			      <h1 className="display-4">Bookswap</h1>
			    </Link>
		    </div>
		    <div className="col-1" style={{"alignSelf": "center"}}>
		          <Link to="./Wish_list">
		          	<h2 className="fas fa-tasks"> </h2>
		          </Link>
		    </div>
		    <div className="col-1"  style={{"alignSelf": "center"}}>
		          <Link to="./Menu">
		          <h2 className="fas fa-user"> </h2>
		          </Link>
		    </div>
		    <div className="col-1"  style={{"alignSelf": "center"}}>
		          <Link to="./Chat">
		          	<h2 className="fas fa-comment-dots"> </h2>
		          </Link>
		    </div>
	    </div>
	</header>
	)
}

export default Header;
