import { Fragment } from 'react'
// import styled from 'styled-components';
import WishListBook from './wishListBook';
import { Droppable } from 'react-beautiful-dnd'

interface Book {
    author: string,
    id: number,
    orderId: number
    status: "done" | "toBeRead",
    title: string,
    userId: number
}
interface Column {
    id: string,
    title: string,
    status: 'toBeRead' | "done",
    bookIds: [] | number[],
}

const WishListColumn: React.FC<{column:Column, books?:(Book | undefined)[] }> = (props) => {

    // console.log('column props', props)

    return (

        <Fragment>
            <div className="col m-2 border">
                <h3 className="p-2">{props.column.title}</h3>
                <Droppable droppableId={props.column.id}>
                    {(provided) => (
                        <div className="p-2"
                        {...provided.droppableProps}
                        ref={provided.innerRef}
                        >
                        {
                            props.books?.map((book, index) =>
                                <WishListBook key={book?.id} book={book} index={index}/>
                            )
                        }
                        {provided.placeholder}
                        </div>
                    )}
                </Droppable>
            </div>
        </Fragment>
    )
}

export default WishListColumn
