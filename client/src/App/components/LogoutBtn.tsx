
import { Link } from 'react-router-dom';
import useLogout from '../hooks/useLogout';

const LogoutBtn = () => {

	const { logoutUser } = useLogout();

	return(

        <Link to='./Login'>
          <button onClick={logoutUser} className="btn btn-secondary">Log out</button>
        </Link>
		)
}

export default LogoutBtn
