import { useEffect, useState } from 'react';
import { DragDropContext, DropResult } from 'react-beautiful-dnd';
import WishListColumn from './wishListColumn';
import axios from 'axios';

interface Book {
    author: string,
    id: number,
    orderId: number
    status: "done" | "toBeRead",
    title: string,
    userId: number
}
interface Column {
    id: string,
    title: string,
    status: 'toBeRead' | "done",
    bookIds: [] | number[],
}

const WishListBody: React.FC<{books: Book[]}> = ({ books }):JSX.Element => {

    const [columns, setColumns] = useState<Column[]>(
        [{
            id: 'col-1',
            title: 'To be read',
            status: 'toBeRead',
            bookIds: [],
        },
        {
            id: 'col-2',
            title: 'Done',
            status: 'done',
            bookIds: []
        }]
    )
    console.log('columns.bookIds', columns)
    const [columnOrder, setColumnOrder] = useState(['col-1', 'col-2'])

    const updateBookIds = (index: number, updatedBooksIds: number[]) => {

        const newArray = [...columns]
        console.log(columns)
        console.log('newArray from updateBooks',newArray)
        console.log('index from updatebookids', index)
        newArray[index].bookIds = updatedBooksIds
        console.log('newArray after',newArray[index])
        setColumns(newArray)
    }

    useEffect(() => {
        columns.forEach((column: Column, index: number) => {
            const filteredBooks = books.filter((book:Book ) => book.status === column.status)
            const bookOrder = filteredBooks.sort((a:Book, b:Book) => (a.orderId > b.orderId) ? 1 : -1)
            let bookIdsArray: number[] = bookOrder.map((el:Book) => el.id)

            console.log('index from useefdfect',index)
            updateBookIds(index, bookIdsArray)

        })
    }, [books])

    const onDragEnd = (result: DropResult): void => {
        console.log('ondragend', result)
        const { destination, source, draggableId } = result

        if (!destination) return
        if (destination.droppableId === source.droppableId && destination.index === source.index) {
            return
        }

        const start = columns.find(el => el.id === source.droppableId)
        const finish = columns.find(el => el.id === destination.droppableId)

        if (start === finish && start !== undefined) {
            const columnIndex = columns.findIndex(col => col.id === source.droppableId)
            const updatedBooksIds = Array.from(start.bookIds)

            updatedBooksIds.splice(source.index, 1);
            updatedBooksIds.splice(destination.index, 0, parseInt(draggableId));

            console.log('updatedBookIds', updatedBooksIds)
            updateIndexes(updatedBooksIds)
            updateBookIds(columnIndex, updatedBooksIds)

        } else if (start !== undefined && finish !== undefined){
            const startIndex = columns.findIndex(col => col.id === source.droppableId)
            const finishIndex = columns.findIndex(col => col.id === destination.droppableId)

            const startColumnBooksIds = Array.from(start.bookIds)
            startColumnBooksIds.splice(source.index, 1);
            console.log('startColumnBookIds', startColumnBooksIds)

            console.log('start index', finishIndex)
            updateBookIds(startIndex, startColumnBooksIds)
            updateIndexes(startColumnBooksIds)

            const finishColumnBooksIds = Array.from(finish.bookIds)
            finishColumnBooksIds.splice(destination.index, 0, parseInt(draggableId));
            console.log('finishColumnBookIds', finishColumnBooksIds)

            console.log('finish index', finishIndex)
            updateBookIds(finishIndex, finishColumnBooksIds)
            updateDatabaseStatus(draggableId, finish.status)
            updateIndexes(finishColumnBooksIds)
        }
    }

    const updateDatabaseStatus = async (bookId:string, status:Book["status"]) => {
        console.log('update database', bookId, status)
            const data = {
                bookId: bookId,
                status: status
            }
        try {
            await axios.post(`http://localhost:5000/updateWishStatus`, data);
        } catch (error) {
            throw new Error(error || 'Could not update your wishlist');
        }
    }

    const updateIndexes = async (indexArray: number[]) => {
        console.log('index array to change', indexArray)
        try {
            await axios.post(`http://localhost:5000/updateIndexes`, indexArray);
        } catch (error) {
            throw new Error(error || 'Could not update your wishlist');
        }
    }

    return (
        <DragDropContext onDragEnd={onDragEnd}>
            {books &&
                columnOrder.map(columnId => {

                    const column = columns.find(el => el.id === columnId)
                    let filteredBooks

                    if (column !== undefined) {
                        filteredBooks = column.bookIds.map(bookId => {
                            for (let i = 0; i < books.length; i++) {
                                if (books[i].id === bookId) return books[i]
                            }
                        })
                       // console.log('filteredBooks', filteredBooks)
                    return <WishListColumn key={columnId} column={column} books={filteredBooks} />
                    }
                })
            }
        </DragDropContext>
    )
}

export default WishListBody;
