import React from 'react';
import { Button } from 'react-bootstrap';
import { Alert } from './Alert';
import { alertService } from '../services/alert.service';

const axios = require('axios').default;

const ReserveBookModal:React.FC<{ bookId: string }> = (props) => {

    console.log("props from reserve book modal")
    console.log(props)

    const onDelete = async () => {
        const bookId = props.bookId
        // console.log(bookId)
        try {
            let res = await axios.get(`http://localhost:5000/updateBook/${bookId}`)
            console.log('res from update book modal')
            console.log(res)
            alertService.success('You reserved this book', {
                autoClose: true,
                keepAfterRouteChange: true
            })
            // update state two parents higher
            //props.bookChange(props.allBooks, props.bookId)
        } catch (error) {
            console.log(error.response)
            alertService.error(error.response.data, { autoClose: true })
        }
    }



    return (
        <div className="modal-body">
            <Button className="yes-btt" type="submit" onClick={onDelete}>Yes</Button>
            {/* <Alert /> */}
        </div>

    )
}

export default ReserveBookModal
