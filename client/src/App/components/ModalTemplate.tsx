import React, { useState, Fragment } from 'react';
import { Button, Modal } from 'react-bootstrap';



const ModalTemplate:React.FC<{ btnName: string | JSX.Element, header: string, ModalBody: JSX.Element}> = (props) => {

  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  // console.log(props)

	return(
		<Fragment>
		      <Button variant="link" onClick={handleShow}>
		        {props.btnName}
		      </Button>
		      <Modal show={show} onHide={handleClose}>
		        <Modal.Header closeButton>
		          <Modal.Title>{props.header}</Modal.Title>
		        </Modal.Header>
		        <Modal.Body>{props.ModalBody}</Modal.Body>
		        <Modal.Footer>
		          <Button variant="secondary" onClick={handleClose}>
		            Close
		          </Button>
		        </Modal.Footer>
		      </Modal>
		</Fragment>
  );
}

export default ModalTemplate
