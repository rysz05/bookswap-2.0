import { Fragment, useState, useContext } from 'react';
import { Prompt } from 'react-router-dom';
import { UserContext } from '../hooks/UserContext';

import LoadingSpinner from './UI/LoadingSpinner';
import classes from './AddBookForm.module.css';
import React from 'react';

interface Book {
	title: string
	author: string
	genre: string
    quote: string
    book_condition: string
    userId: number
}

const AddBookForm:React.FC<{ onAddBook: (bookData:Book)=> void, isLoading: boolean}> = props => {

    console.log('addbookform', props)
    const [isEntering, setIsEntering] = useState(false)
    const context = useContext(UserContext)

    const initialUserState = {
        id: null,
        author: '',
        title: '',
        quote: '',
        genre: '',
        book_condition: '',
        userId: context!.user!.id
    };
    const [book, setBook] = useState(initialUserState);
    const genre = ['Romance', 'Drama', 'Essays', 'Sci-fy', 'Fantasy', 'Horror', 'Action']

    const handleInputChange = (event:React.FormEvent<HTMLInputElement> | React.FormEvent<HTMLSelectElement> | React.FormEvent<HTMLTextAreaElement>) => {

        console.log(event.currentTarget.value)

        const { name, value } = event.currentTarget;
        setBook({ ...book, [name]: value });

    };

    function submitFormHandler(event:React.FormEvent) {
        event.preventDefault();
        console.log('submit form handler', book)
        props.onAddBook(book);

        //sendRequest(book)
    }

    const finishEnteringHandler = () => {
        setIsEntering(false);
    };

    const formFocusHandler = () => {
        setIsEntering(true);
    };

    return (
        <Fragment>
            <Prompt
                when={isEntering}
                message={(location) => 'Are you sure you want to leave, all your input will be lost.'}
            />
            <form
                onFocus={formFocusHandler}
                onSubmit={submitFormHandler}
                className="form-cont form-group"
            >
                {props.isLoading && (
                    <div className={classes.loading}>
                        <LoadingSpinner />
                    </div>
                )}
                <h3>By hand</h3>
                <div className="form-group row">
                    <label htmlFor="author" className="col-sm-2 col-form-label">Author</label>
                    <div className="autocomplete col-sm-10">
                        <input
                            type="text"
                            id="myInput"
                            name="author"
                            value={book.author}
                            onChange={handleInputChange}
                            className="text-input form-control" />
                    </div>
                </div>
                <div className="form-group row">
                    <label htmlFor="title" className="col-sm-2 col-from-label">Title</label>
                    <div className="col-sm-10">
                        <input
                            type="text"
                            id="title"
                            name="title"
                            value={book.title}
                            onChange={handleInputChange}
                            className="text-input form-control col" />
                    </div>
                </div>
                <select
                    onChange={handleInputChange}
                    className="browser-default custom-select"
                    name='genre'>
                    {
                        genre.map((genre, key) =>
                            <option key={key} value={genre}>{genre}</option>
                        )
                    }
                </select >
                <div className="radio-box form-group">
                    <label htmlFor="condition">Book condition</label>
                    <div className="radio-container">
                        <input
                            id="book-condition-good"
                            name="book_condition"
                            type="radio"
                            onChange={handleInputChange}
                            value="good" />
                        <label htmlFor="book-condition-good">Good</label>
                        <input
                            id="book-condition-medium"
                            name="book_condition"
                            type="radio"
                            onChange={handleInputChange}
                            value="medium" />
                        <label htmlFor="book-condition-medium">More or less</label>
                        <input
                            id="book-condition-bad"
                            name="book_condition"
                            type="radio"
                            onChange={handleInputChange}
                            value="bad" />
                        <label htmlFor="book-condition-bad">Hmmm..</label>
                    </div>
                </div>
                <div className="form-group">
                    <textarea
                        className="form-control"
                        name="quote"
                        onChange={handleInputChange}
                        rows={4}
                        placeholder="Here You can give Your unwanted opinion on the book">
                    </textarea>
                </div>
                <div className="col">
                    <button onClick={finishEnteringHandler} type="submit" className="btn btn-primary">Add Book</button>
                </div>
            </form>
        </Fragment>
    )
}

export default AddBookForm
