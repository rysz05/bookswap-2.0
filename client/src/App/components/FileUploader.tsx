
const FileUploader:React.FC<{onFileSelectSuccess:any, onFileSelectError:any}> = ({onFileSelectSuccess, onFileSelectError}) => {
	//const fileInput = useRef(null)


	const handleFileInput = (e:React.FormEvent<HTMLInputElement>) => {
		// handle validations
		const file = e.currentTarget.files![0]
		let size = parseFloat((file.size / (1024 * 1024)).toFixed(2))
		if (size > 2) {
			onFileSelectError({ error: "File size cannot exceed more than 1MB"});
		}
		else {
			onFileSelectSuccess(file)
		}

	}

	return (
		<div className="file-uploader">
			<input name="img_path" type="file" onChange={handleFileInput} />
			{/*<button onClick={e => fileInput.current && fileInput.current.click()} className="btn btn-primary">Add picture</button>*/}
		</div>
		)
}

export default FileUploader
