import React from 'react';
import { Button } from 'react-bootstrap';
// import { Alert } from './Alert';
import { alertService } from '../services/alert.service';
const axios = require('axios').default

const ReserveBookButton:React.FC<{ bookId: string, status: string, update:()=> void }> = (props) => {

    const onUpdate = async () => {

        const request = {
            bookId: props.bookId,
            status: props.status
        }
        console.log(request)
        try {
            await axios.post(`http://localhost:5000/updateBook`, request)

            // let alertInfo = props.status === "free" ? 'You reserved this book' : 'The book is no longer reserved'

            // alertService.success(alertInfo, {
            //     autoClose: true,
            //     keepAfterRouteChange: true
            // })

            // update state two parents higher
            props.update()
        } catch (error) {
            console.log(error.response)
            alertService.error(error.response, { autoClose: true })
        }
    }

    return (

        <React.Fragment>
            <Button onClick={onUpdate} className="btn btn-secondary">
                {props.status === "free" ? 'Reserve' : 'Un - reserve'}
            </Button>
            {/* <Alert /> */}
        </React.Fragment>
    )
}

export default ReserveBookButton
