import React, { useState } from 'react';
import { Button } from 'react-bootstrap';
const axios = require('axios').default;

const AddressChangeModalBody:React.FC<{ update:()=> void, id: number, city: string, street: string, building_nr: string}> = (props) => {

	const [address, setAddress] = useState({
		id: props.id,
		city: props.city,
		street: props.street,
		building_nr: props.building_nr
	})

	const handleInputChange = (event:React.FormEvent<HTMLInputElement>) => {

		const { name, value } = event.currentTarget;
		setAddress({ ...address, [name]: value });
	};

	const updateAddress = async (event:React.FormEvent<HTMLFormElement>) => {
		event.preventDefault()

		await axios.post('http://localhost:5000/update', address)
		props.update()
		console.log(address)

	}

	return(
		<form onSubmit={updateAddress}>
			<div className="form-group">
				<label htmlFor="city">city</label>
				<input
					type="city"
					className="form-control"
					name="city"
					value={address.city}
					onChange={handleInputChange}
					required={true}  />
			</div>
			<div className="form-group">
				<label htmlFor="street">street</label>
				<input
					type="street"
					className="form-control"
					name="street"
					value={address.street}
					onChange={handleInputChange}
					required={true} />
			</div>
			<div className="form-group">
				<label htmlFor="building_nr">building number</label>
				<input
					type="building_nr"
					className="form-control"
					name="building_nr"
					value={address.building_nr}
					onChange={handleInputChange}
					required={true} />
			</div>
			<Button type='submit' variant="primary">
		        Update address
		    </Button>
		</form>
		)
}

export default AddressChangeModalBody
