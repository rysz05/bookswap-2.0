import React, { useState } from 'react';
import { Button } from 'react-bootstrap';
import FileUploader from "./FileUploader";
import { Alert } from './Alert';
import { alertService } from '../services/alert.service';

const axios = require('axios').default;

const PersoDataChangeModalBody:React.FC<{ phone_nr:string, email: string, img_path: any, id: string, update:()=>void }> = (props) => {

	console.log('persodatachangemodalbody', props)

	const [selectedFile, setSelectedFile] = useState<string | Blob>('')

	const [info, setInfo] = useState({
		phone_nr: props.phone_nr,
		email: props.email,
		img_path: props.img_path,
	})
	console.log(info)

	const handleInputChange = (event:React.FormEvent<HTMLInputElement>) => {
		const { name, value } = event.currentTarget;
		setInfo({ ...info, [name]: value });
		console.log('ingo', info)
	};

	const updateInfo = async () => {

		let formData = new FormData();

		formData.append("id", props.id)
		formData.append("email", info.email)
		formData.append("phone_nr", info.phone_nr)
		formData.append("img_path", selectedFile)

		console.log(formData)
		try {

			let res = await axios.post('http://localhost:5000/update', formData)
			console.log(res)
			alertService.success('You removed your book', {
				autoClose: true,
				keepAfterRouteChange: true
			})
		} catch (error) {
			console.log(error)
		}
	}

	return(
		<form>
			{/* <Alert /> */}
			<div className="form-group">
			    <FileUploader
					// img_path={props.img_path}
			       	onFileSelectSuccess={(file:any) => setSelectedFile(file)}
			       	onFileSelectError={(error:string) => alert(error)}
				/>
			</div>
			<div className="form-group">
				<label htmlFor="phone_nr">phone_nr</label>
				<input
					type="phone_nr"
					className="form-control"
					name="phone_nr"
					value={info.phone_nr}
					onChange={handleInputChange}
					required={true} />
			</div>
			<div className="form-group">
				<label htmlFor="email">email</label>
				<input
					type="email"
					className="form-control"
					name="email"
					value={info.email}
					onChange={handleInputChange}
					required={true} />
			</div>
			<Button type="submit" variant="primary" onClick={updateInfo}>
		        Update info
		    </Button>
		</form>
		)
}

export default PersoDataChangeModalBody
