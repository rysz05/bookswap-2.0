import React from 'react';
import { Button } from 'react-bootstrap';
import { Alert } from './Alert';
import { alertService } from '../services/alert.service';

const axios = require('axios').default;

const DeleteBookModal:React.FC<{ bookId: string, update: ()=> void}> = (props) => {

console.log("props from delete book modal")
console.log(props)

	const onDelete = async () => {
		const bookId = props.bookId
		// console.log(bookId)
		try {
			let res = await axios.post(`http://localhost:5000/deleteBook/${bookId}`)
			console.log('res from delete book modal')
			console.log(res)
			   alertService.success('You removed your book', {
			   		autoClose: true,
			   		keepAfterRouteChange: true
			   	})
			// update state two parents higher
			props.update()
		} catch (error) {
			console.log(error.response)
			alertService.error(error.response, {autoClose: true})
		}
	}



	return (
			<div className="modal-body">
				<Button className="yes-btt" type="submit" onClick={onDelete}>Yes</Button>
				{/* <Alert /> */}
			</div>

		)
}

export default DeleteBookModal
