import React from 'react';
import ModalTemplate from './ModalTemplate';
import DeleteBookModal from './DeleteBookModal';
import ReserveBookButton from './ReserveBookButton';

const BookCard:React.FC<{ id: string, title: string, author: string, genre: string, status: "free" | "reserved", update: ()=> void}> = (props) => {

	// console.log("props book card")
	console.log('props book card', props)

	return (
	<div className="card book" id={props.id}>
		<div className="card-body">
			<div className="card-title">{props.title}</div>
			<div className="card-subtitle text-muted">{props.author}</div>
			<div className="card-text">{props.genre}</div>
		</div>
			<ModalTemplate
				btnName="X"
				header="Are you sure you want to remove this book?"
				ModalBody={<DeleteBookModal
							bookId={props.id}
							update={props.update}
							/>}
			/>
			<ReserveBookButton
				bookId={props.id}
				status={props.status}
				update={props.update}
				/>
		</div>
	)
}

export default BookCard;
