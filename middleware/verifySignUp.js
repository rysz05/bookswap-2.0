const db = require ("../models")
const ROLES = db.ROLES;
const User = db.user;

checkDuplicateUsernameOrEmail = (req, res, next) => {

console.log("check duplicates")
console.log(req.body)
console.log(req.body.username)
	// USERNAME
	User.findOne({
		where: {
			username: req.body.username
		}
	}).then(user => {
		if (user) {
			return res.status(400).send("Username already in use :(")
		}

		User.findOne({
			where: {
				email: req.body.email
			}
		}).then(user => {
			if (user) {
				return res.status(400).send("Email already in use")
			}
			next();
		})
	})
}

const verifySignUp = {
	checkDuplicateUsernameOrEmail: checkDuplicateUsernameOrEmail,
};

module.exports = verifySignUp;
