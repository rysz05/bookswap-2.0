const db = require("../models");
// const User = db.user;
const Book = db.book;
const Wish = db.wishListBook;
const Op = db.Sequelize.Op;
const { asyncWrapper } = require('../middleware/async-handler.js')
// const auth = './authController';


exports.getAllBooks = asyncWrapper(async (req, res) => {

	const userId = parseInt(req.params.userId)
	// console.log(userId)
	const allBooks = await Book.findAll({
		where: {
			[Op.not]: [
				{ userId: userId },
				{status: "reserved"}
			]
		}
	});
	// console.log(allBooks);
	res.send(allBooks)
})

exports.loadSuggestions = asyncWrapper(async (req, res) => {

	const userId: string = req.params.userId;
	const userInput: string = req.params.input;

	const allBooks = await Book.findAll({
			where: {
					[Op.or]: {
						title: {[Op.substring]: userInput},
						author: { [Op.substring]: userInput }
					},
					[Op.not]: [
						{userId: userId },
						{status: 'reserved'}
					]
			}
		});
		// console.log(allBooks);
	res.send(allBooks)
})

exports.getUsersBooks = asyncWrapper(async (req, res) => {
	const id: string = req.params.id
	// console.log(req.params)
	const results = await Book.findAll({
			where: {
				userId: id
			}
	})
	console.log(results)
	res.send(results)
})

exports.getAskResult = asyncWrapper(async (req, res) => {

	const ask: string = req.params.ask
	const userId = parseInt(req.params.userId)
	// console.log(req.params)
	const results = await Book.findAll({
		where: {
			[Op.and]: {
				[Op.or]: [{ author: ask }, { title: ask }],
				[Op.not]: [{ userId: userId }]
			}
		}
	})
	// console.log(results)
	res.send(results)
})

exports.addBook = asyncWrapper(async (req, res) => {

	if (req.body.author || req.body.title) {
		const book = await Book.create(req.body)
		console.log(book)
		console.log("controller after create new book")
		return res.sendStatus(201).json({book})
	} else {
		return res.sendStatus(400).send('You have to include both authors name, and title');
	}
})

exports.deleteBook = asyncWrapper(async (req, res) => {

	// console.log(req.params.bookId)
	const bookId = req.params.bookId
	let resp = await Book.destroy({
		where: {
			id: bookId
		}
	})
	// console.log(resp)
	return res.sendStatus(200).send(resp)
})

exports.updateBook = asyncWrapper(async (req, res) => {

	// console.log(req.body)
	let newStatus = req.body.status === "free" ? "reserved" : "free"
	await Book.update({ status: newStatus }, {
		where: {
			id: req.body.bookId
		}
	})
	// console.log(resp)
	return res.sendStatus(200)
})

interface Wish {
	userId: number,
    author: string,
    title: string,
    orderId: number
}

exports.postWish = asyncWrapper(async (req: any, res) => {
	// console.log(req.body)
	if (req.body.author || req.body.title) {
		const book = await Wish.create(req.body)
		// console.log(book)
		// console.log("controller after create new wish")
		res.send(book)
	} else {
		return res.sendStatus(400).send('You have to include both authors name, and title');
	}
})

exports.updateWishStatus = asyncWrapper(async (req, res) => {

	//console.log(req.body)
	await Wish.update({ status: req.body.status }, {
			where: {
				id: req.body.bookId
			}
		})
	return res.sendStatus(200)
})

exports.updateIndexes = (req, res) => {

	let indexesArray = req.body
	// console.log(req.body)

	indexesArray.forEach((id, index) => {
		asyncWrapper(runUpdates(id, index))
	});
	return res.sendStatus(200)

	async function runUpdates(id, index) {
		//console.log(index, id)
		await Wish.update({ orderId: index }, {
				where: {
					id: id
				}
			})
	}
}

exports.getUsersWishList = asyncWrapper(async (req, res) => {
	const id = req.params.userId
	// console.log(req.params)
	const results = await Wish.findAll({
		where: {
			userId: id
		}
	})
	// console.log(results)
	res.send(results)
})
