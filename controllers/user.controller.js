const db = require("../models");
const User = db.user;
const auth = './authController';
//const Op = db.Sequelize.Op;
const geo = require('../tools/distance.js');
const { asyncWrapper } = require('../middleware/async-handler.js')

const errHandler = (err) => {
	console.error("Error: ", err)
}

exports.allAccess = (req, res) => {
  res.status(200).send("Public Content.");
};

exports.userBoard = (req, res) => {
  res.status(200).send("User Content.");
};

exports.create = asyncWrapper(async (req, res) => {
// console.log("hi")
	if (req.body.password || req.body.email || req.body.username) {
		const newUser = {
				username: req.body.username,
				email: req.body.email,
				password: req.body.password,
				phone_nr: req.body.phone_nr,
				image_path: req.body.image_path,
				city: req.body.city,
				street: req.body.street,
				building_nr: req.body.building_nr
			};

		//console.log(newUser)
		const user = await User.create(newUser)
		// console.log("controller after create")
		return res.status(201).json({user})
	} else {
		return res.status(400).send('You have some empty input fields');
	}
})

exports.findUsersData = asyncWrapper(async (req, res) => {

	// console.log(req.params)
	let id = req.params.id
	// console.log(id)
	const data = await User.findByPk(id)
	// console.log(data)
	const toSendBack = {
		username: data.username,
		email: data.email,
		img_path: data.img_path,
		phone_nr: data.phone_nr,
		city: data.city,
		street: data.street,
		building_nr: data.building_nr,
		lat: data.lat,
		lng: data.lng
	}
	res.send(toSendBack)
});

exports.update = asyncWrapper(async (req, res) => {

	// console.log('req.body', req.body)
	const changedData = req.body
	// console.log('changedData', changedData)
	// console.log('req.files', req.files)
	// TO CHANGE USER ADDRESS
	if (changedData.street || changedData.city || changedData.building_nr) {
		let address = `${changedData.street} ${changedData.building_nr}, ${changedData.city}`
		let coordinates = await geo.forwardGeocoding(address)
		if (coordinates) {
			changedData.lat = coordinates.lat
			changedData.lng = coordinates.lng
			console.log(changedData)
		}
	}
		// TO CHANAGE USERS PROFILE PIC
		// if (req.files) {
        //   changedData.img_path = req.files[0].path
        // }

	await User.update(changedData, {
		where: {id: req.body.id}
		})
})
