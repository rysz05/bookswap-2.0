const db = require("../models");
const User = db.user;
//const AppError = require('./../utils/AppError');
const geo = require('../tools/distance.js');
const jwt = require('jsonwebtoken');
let bcrypt = require("bcryptjs");
const { promisify } = require('util');
const { asyncWrapper } = require('../middleware/async-handler.js')

console.log(asyncWrapper)


//sign JWT token for authenticated user
const signToken = id => {

   return jwt.sign({ id }, process.env.JWT_SECRET, {
          expiresIn: process.env.JWT_EXPIRES_IN
   });
}


//create JWT token for authenticated user
const createUserToken = async(user, code, req, res) => {

    const token = signToken(user.id);
    //remove user password from output for security
    user.password = undefined;

    res.status(code).json({
       status: 'success',
       token,
       data: {
          user
        }
      });
    };

//create new user
exports.registerUser = asyncWrapper(async(req, res, next) => {
  console.log("register user")
     //pass in request data here to create user from user schema
  let user_data = req.body
  // console.log(user_data)
  // console.log(req.files)
  let address = `${user_data.street} ${user_data.building_nr}, ${user_data.city}`
  user_data.password = bcrypt.hashSync(user_data.password, 8)

  let coordinates = await geo.forwardGeocoding(address)
  if (coordinates) {
    user_data.lat = coordinates.lat
    user_data.lng = coordinates.lng
    }
  // console.log(req.files)
  if (req.files.length) {
    user_data.img_path = req.files[0].path
   }
   // console.log(user_data)

  const newUser = await User.create(user_data);
  createUserToken(newUser, 201, req, res);
});


//log user in
exports.loginUser = asyncWrapper(async(req, res, next) => {
     const { username, password } = req.body;

    //check if user & password are correct
   const user = await User.findOne({
      where: {username: username}})

    if (!user) {
      return res.status(404).send({ message: "User Not found." });
    }

    var passwordIsValid = await bcrypt.compare(
          password,
          user.password
    );

    if (!passwordIsValid) {
      return res.status(401).send({
        accessToken: null,
        message: "Invalid Password!"
      });
    }
    createUserToken(user, 200, req, res);
});


//check if user is logged in
exports.checkUser = asyncWrapper(async (req, res, next) => {
    let currentUser;

    if (req.cookies.jwt) {
       const token = req.cookies.jwt;

       const decoded = await promisify(jwt.verify)(token, process.env.JWT_SECRET);
       currentUser = await User.findOne({
         where: {id : decoded.id}
        })
       currentUser = {
         id: currentUser.id,
         username: currentUser.username
       }

   } else {
     currentUser =  null;
  }
   res.status(200).send({ currentUser });
})
