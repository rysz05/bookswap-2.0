const { book } = require("../models");
const db = require("../models");
const Chat = db.chat;
const Book = db.book;
const { asyncWrapper } = require('../middleware/async-handler.js')

exports.createChat = asyncWrapper(async (req, res) => {

    console.log("controller before create new chat")
    console.log(req.body)

    const chat = await Chat.create(req.body)
        if (chat) {
            await Book.update({ status: "reserved" }, {
                where: {
                    id: req.body.bookId
                }
            })
        }
    // console.log("controller after create new chat")
    return res.sendStatus(201).json({ chat })
})

exports.getChat = asyncWrapper(async (req, res) => {

    const chatId = req.params.chatId
    //console.log(req.params)
    const results = await Chat.findAll({
        where: {
            id: chatId
        }
    })
    console.log(results)
    res.send(results)
})
