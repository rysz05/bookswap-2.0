const db = require("../models");
// const User = db.user;
const Book = db.book;
const Wish = db.wishListBook;
const Op = db.Sequelize.Op;
// const auth = './authController';


exports.getAllBooks = async (req, res) => {

	const userId = parseInt(req.params.userId)
	console.log(userId)
	try {
		const allBooks = await Book.findAll({
			where: {
				[Op.not]: [
					{ userId: userId },
					{status: "reserved"}
				]
			}
		});
		console.log(allBooks);
		res.send(allBooks)
	} catch (err) {console.log(err)}
}

exports.loadSuggestions = async (req, res) => {

	const userId = req.params.userId;
	const userInput = req.params.input;

	try {
		const allBooks = await Book.findAll({
			where: {
					[Op.or]: {
						title: {[Op.substring]: userInput},
						author: { [Op.substring]: userInput }
					},
					[Op.not]: [
						{ userId: userId },
						{status: 'reserved'}
					]
			}
		});
		console.log(allBooks);
		res.send(allBooks)
	} catch (err) { console.log(err) }
}

exports.getUsersBooks = async (req, res) => {
	const id = req.params.id
	console.log(req.params)
	try {
		const results = await Book.findAll({
				 where: {
				    userId: id
				  }
		})
		console.log(results)
		res.send(results)
	} catch (err) {console.log(err)}
}

exports.getAskResult = async (req, res) => {

	const ask = req.params.ask
	const userId = parseInt(req.params.userId)

	console.log(req.params)
	try {
		const results = await Book.findAll({
			where: {
				[Op.and]: {
					[Op.or]: [{ author: ask }, { title: ask }],
					[Op.not]: [{ userId: userId }]
				}
			}
		})
		console.log(results)
		res.send(results)
	} catch (err) {console.log(err)}
}

exports.addBook = async (req, res) => {

	if (req.body.author || req.body.title) {
		try {
			const book = await Book.create(req.body)
			console.log(book)
			console.log("controller after create new book")
			return res.sendStatus(201).json({book})
		} catch (error) {
			// return res.sendStatus(500).send('Server error')
			console.log(error)
		}
	} else {
		return res.sendStatus(400).send('You have to include both authors name, and title');
	}
}

exports.deleteBook = async (req, res) => {

	// console.log(req.params.bookId)
	const bookId = req.params.bookId
	try {
		let resp = await Book.destroy({
			where: {
				id: bookId
			}
		})
		console.log(resp)
		return res.sendStatus(200).send(resp)
	} catch (error) {
		console.log('hello')
		return res.sendStatus(500).send('Server error')
	}
}

exports.updateBook = async (req, res) => {

	console.log(req.body)

	let newStatus = req.body.status === "free" ? "reserved" : "free"
	try {
		await Book.update({ status: newStatus }, {
			where: {
				id: req.body.bookId
			}
		})
		// console.log(resp)
		return res.sendStatus(200)
	} catch (error) {
		console.log('hello')
		return res.sendStatus(500).send('Server error')
	}
}

exports.postWish = async (req, res) => {
	console.log(req.body)
	if (req.body.author || req.body.title) {
		try {
			const book = await Wish.create(req.body)
			console.log(book)
			console.log("controller after create new wish")
			res.send(book)

		} catch (error) {
			// return res.sendStatus(500).send('Server error')
			console.log(error)
		}
	} else {
		return res.sendStatus(400).send('You have to include both authors name, and title');
	}
}

exports.updateWishStatus = async (req, res) => {

	console.log(req.body)

	try {
		await Wish.update({ status: req.body.status }, {
				where: {
					id: req.body.bookId
				}
			})
		return res.sendStatus(200)
	} catch (error) {
		console.log('hello')
		return res.sendStatus(500).send('Server error')
	}
}

exports.updateIndexes = (req, res) => {

	let indexesArray = req.body
	console.log(req.body)

	indexesArray.forEach((id, index) => {
		runUpdates(id, index)
	});
	return res.sendStatus(200)

	async function runUpdates(id, index) {
		console.log(index, id)
		try{
			await Wish.update({ orderId: index }, {
				where: {
					id: id
				}
			})
		} catch (error) {
			return res.sendStatus(500).send('Server error')
		}
	}



	// try {
	// 	await Wish.update({ status: req.body.status }, {
	// 		where: {
	// 			id: req.body.bookId
	// 		}
	// 	})
	// 	return res.sendStatus(200)
	// } catch (error) {
	// 	return res.sendStatus(500).send('Server error')
	// }
}

exports.getUsersWishList = async (req, res) => {
	const id = req.params.userId
	console.log(req.params)
	try {
		const results = await Wish.findAll({
			where: {
				userId: id
			}
		})
		console.log(results)
		res.send(results)
	} catch (err) { console.log(err) }
}
